var gulp = require('gulp'), // Prijungiame Gulp
sass = require('gulp-sass'); // Prijungiame Sass paketa

gulp.task('sass', function() { // Sukuriame taska Sass
return gulp.src(['includes/sass/**/*.sass', 'includes/sass/**/*.scss']) // Imame saltini
.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) // Konvertuojame sass i css naudojant gulp-sass
.pipe(gulp.dest('includes/css')) // Isloadiname rezultata i css folderi
});

gulp.task('watch', function() {
gulp.watch(['includes/sass/**/*.sass', 'includes/sass/**/*.scss'], ['sass']); // Stebejimas sass failu, sass folderyje
});

gulp.task('default', ['watch']);
