# Weather App Landing Page

<p align="center"><img src="https://user-images.githubusercontent.com/37780449/38111194-c32efa72-33a6-11e8-9623-0d41ca736be4.png"></p>

## About

Simple and beautiful Weather App responsive design

## Screenshots

![screenshot_2](https://user-images.githubusercontent.com/37780449/42262930-52f0d922-7f75-11e8-8539-16ba0f4f14ba.jpg)
![screenshot_5](https://user-images.githubusercontent.com/37780449/42262986-744b6c72-7f75-11e8-8660-c7ed9f650c76.jpg)
![screenshot_7](https://user-images.githubusercontent.com/37780449/42263057-a48e59d0-7f75-11e8-93be-dccdee93a0df.jpg)
![screenshot_4](https://user-images.githubusercontent.com/37780449/42262940-566c903c-7f75-11e8-82e3-1cd9c021ceca.jpg)
