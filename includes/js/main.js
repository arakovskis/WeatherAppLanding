// Scroll To Top Button FadeIn and FadeOut After 50px
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {
        $('#return-top-btn').fadeIn(200);
    } else {
        $('#return-top-btn').fadeOut(200);
    }
});

// Scrolling To Top Function
function returnTop() {
    $('html, body').animate({
        scrollTop:0
    }, 800);
};

// Scroll Down To Second Section From Hero
function scrollDown() {
    const visisbleEl = $('.reviews').is(":visible") ? 'reviews' : 'features';

    $('html, body').animate({
        scrollTop: $("." + visisbleEl).offset().top
    }, 1000);
}



window.onload = function() {
    $('.nav-link').on('click', function(e) {
        const id = e.target.id;
        
        if (id === 'features') {
            $('.reviews').hide();
            $('.features').show();
        } else if (id === 'reviews') {
            $('.features').hide();
            $('.reviews').show();
        }

        $('html, body').animate({
            scrollTop: $('.' + id).offset().top
        }, 1000);
    }); 
}